% neurospec_trigger_sending_demo
%
% This script contains an example of how to send EEG event codes (i.e.,
% trigger codes) via serial port via the Neurospec USB to Parallel Port
% interface. It has been tested on a mac, but in principle it should work
% on Windows devices as well. Very similar code can also be used to send
% triggers from BrainProducts devices as well. 
%
% Written by Daniel Feuerriegel, 4/19 at the University of Melbourne. Huge
% thanks to Ryan Maloney who originally co-developed the code with Daniel
%
% This code relies on some functions from Psychtoolbox, so if you get
% errors then check if these are associated with certain PTB functions. If
% you don't have Psychtoolbox, then you may be able to write code that
% mimics these functions (in particular WaitSecs).
%
% If using a mac, get the required drivers from:
% 
% http://www.prolific.com.tw/US/ShowProduct.aspx?%20id=229&pcid=41
% 
% Or try and install the drivers included in the repository


% To see a list of available ports, use the function below originally written by Daniel Lavoie. 
% This has been modified to see devices for Windows PCs and Macs. You can find a similar
% version of the code on the MATLAB File Exchange.

listOfCOMPorts =  getAvailableComPort();


% Once you have identified the string associated with the serial port you
% are using, store this in a structure.



%% Serial Port Settings
% Here we determine the serial port to use, whether to send EEG triggers
% (turn off if debugging), the duration of the event code pulse.

Port.inUse = true;         % set to true if sending triggers over serial port
Port.COMport = '/dev/tty.usbmodem1411';     % the COM port for the trigger box
Port.EventTriggerDuration = 0.008; % Duration, in sec, of trigger; delay before the port is flushed and set back to zero


% NOTE: In the default 'Pulse' mode the trigger duration is always 8ms, and
% after 8ms the triggers always reset to 0, even without any explicit
% instructions from the stimulation computer. If two triggers are sent
% within 8ms, the USB to parallel port converter will wait out the first
% 8ms before sending the next trigger! You can change this by unscrewing
% the box and changing the hardware jumper setting. Do NOT do this unless
% you really know what you are doing, and mark that you have changed this
% on the outside of the unit!



%% Open the serial device for triggers (if using)

% Note that this section relies on the accompany function
% send_event_trigger_neurospec

if Port.inUse
    
    Port.sObj = serial(Port.COMport); % Make a MATLAB object with serial port info
    
    fopen(Port.sObj); % Open the serial port
    
    % Send a dummy pulse to initialise the device:
    send_event_trigger_neurospec(Port.sObj, Port.EventTriggerDuration, 255) % Send an event code 255
    
else
    
    Port.sObj = []; % Just make an empty object if we're not using the port

end


% Wait a little bit before sending the next trigger below (useful for testing
% the code to see if each trigger appears)
WaitSecs(2);

%% Sending EEG Event Codes / Triggers

% Set event code to send to be number 100
eventCodeNo_temp = 100;


if Port.inUse % If sending EEG triggers
    
    % Send an event code
    send_event_trigger_neurospec(Port.sObj, Port.EventTriggerDuration, eventCodeNo_temp) 
    
end % of if Port.inUse




%% At End of Experiment

% Close the serial port:
fclose(Port.sObj);

% Get rid of whole struct:
clear('Port')




