# eeg-triggers-starter-pack

This repository contains code examples and functions to help you get started with sending event codes (also known as 'triggers') to the EEG amplifier. Event codes are sent to signify the timing of critical events (e.g., stimulus onset or time of response), and can also be used to signal relevant trial characteristics (e.g., trial type, trial number in block, block number etc.) 

Each function is prefixed by **EXAMPLE**, and you should change this to be the name of your experiment. This avoids unintentional sharing of MATLAB functions across experiments, which can lead to others unintentionally changing how your experiment runs when they modify the code for a given function.

The duration of each event code is hard-coded in the EEG port setup functions. It is by defualt 2ms (just over a single sample duration when using a 512Hz sampling rate on the Biosemi systems). If you use a lower sampling rate you should extend this to cover at least one sample duration in your EEG recordings. If you don't know what this means, ask your local EEG expert in the lab.

The code for using the Neurospec MMB devices is in the Neurospec folder. These devices use the serial port functions in MATLAB, and so use a different set of commands to when using the parallel port or National Instruments devices. Note that the Neurospec/MMB devices have some peculiarities (e.g., default setting of 8ms event code durations built into the firmware). Check the manuals supplied with these devices to be aware of their peculiarities. 

Repository originally created by Daniel Feuerriegel, 4/19 at the University of Melbourne. Extra contributions by ... 

A very special thanks to Ryan Maloney, who wrote the original code for sending triggers via the serial port. This code can also be adapted for use with BrainProducts systems in some cases. 