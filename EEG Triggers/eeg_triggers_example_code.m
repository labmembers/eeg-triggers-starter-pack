% This script shows examples of using MATLAB code to set up the EEG ports,
% and send event codes/triggers to the EEG system during the experiment
%
% First, we set up the ports to enable trigger sending. Next, we send EEG
% event codes marking critical events in the experiment. At the end of the
% experiment, we close/reset the ports.



%% EEG Port Switch Variables

% These variables turn off/on trigger sending, and determine the method of
% sending EEG triggers (parallel port or USB-to-parallel-port interface)

% Enable sending triggers via parallel port to EEG amplifier?
Port.isOn = 0; % Set to 1 when sending EEG triggers (0 = Off / 1 = On)
Port.type = 'USB'; % Set 'Parallel' to use parallel port directly, or 'USB' when using USB interface




%% EEG Trigger Port Setup

% Setup depends on whether using parallel port directly or through USB
if Port.isOn
    
    if strcmp(Port.type, 'USB') % If using USB to parallel port converter
        
        [Port, s, ch] = EXAMPLE_set_up_eeg_port_usb(Port);
        
    elseif strcmp(Port.type, 'Parallel') % If using parallel port directly
    
        Port = EXAMPLE_set_up_eeg_port(Port);
        s = []; % Create dummy variable s so this works with event code sending function
        
    end % of if strcmp Parallel/USB
        
    
else % If EEG ports not used then create dummy variables for use with experiment functions
    
    s = [];
    
end % of if Port.isOn




%% Send EEG Trigger During Experiment

% Send trigger denoting sequence number in the experiment
EXAMPLE_send_event_codes(Port, eventCodeNo, s);




%% Close EEG Ports at End of Experiment

% Close EEG ports
if Port.isOn

    EXAMPLE_close_eeg_ports(Port, s);

end % of if Port.isOn
