function send_eeg_event_codes(Port, eventCodeNo, s)
%
% This function sends EEG event codes via either the USB interface of the
% parallel port.
%
% Inputs:
%   
%   Port    Structure containing variables relating to sending of event
%           codes to the EEG
%
%   eventCode   A number (between 1 - 255) signifying the event code to
%               send to the EEG system. 
%
%   s       Object which contains information about how to interface with
%           the EEG hardware (vital when using USB-to-parallel port interface)
% 
% 
% Outputs:
%
% None
%
% Written by Daniel Feuerriegel, 3/19 at the University of Melbourne


if Port.isOn
            
    if strcmp(Port.type, 'USB') % If using USB to parallel port converter

        theta_waffles_send_port_codes_usb(s, eventCodeNo, Port.pulseDuration)

    elseif strcmp(Port.type, 'Parallel') % If using parallel port directly

        theta_waffles_send_port_codes(Port.ioObj, Port.address, eventCodeNo, Port.pulseDuration)

    end % of if strcmp Parallel/USB

end % of if Port.isOn