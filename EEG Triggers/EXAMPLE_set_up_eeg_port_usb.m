function [Port, s, ch] = set_up_eeg_port_usb(Port)
% 
% This function sets up the parallel port for sending EEG triggers to the
% Biosemi EEG system (or whatever system is being used). This function sets
% up the USB to parallel port interface, and creates the objects 's' and
% 'ch' for this purpose.
%
% Note that the event code sending duration is defined within this function
%
% For setting up the parallel port without using a USB interface see the
% function set_up_eeg_port
%
%
% Inputs:
%   
%   Port    Structure containing variables relating to sending of event
%           codes to the EEG
%
%   s       Object which contains information about how to interface with
%           the EEG hardware (vital when using USB-to-parallel port interface)
% 
% 
% Outputs:
%
%   Port    Structure containing variables relating to sending of event
%           codes to the EEG
%
%   s, ch   Objects relating to interfacing with the USB-to-parallel port
%           interface hardware
%
%
% Written by Daniel Feuerriegel for the Change of Mind project at the
% University of Melbourne
% 
% 
% 
        
s = daq.createSession('ni'); % Initialise the session

ch = addDigitalChannel(s, 'Dev1', 'Port2/Line0:7', 'OutputOnly'); % Setup 8-bit range of triggers

Port.pulseDuration = 0.002; % Duration of triggers for Biosemi

% Reset triggers to zero (in case someone left parallel ports open)
outputSingleScan(s, [0 0 0 0 0 0 0 0]);