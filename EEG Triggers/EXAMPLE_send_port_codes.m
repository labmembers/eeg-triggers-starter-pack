function send_port_codes(ioObj, address, eventCode, pulseDuration)
%
% Sends a trigger code via the parallel port
%
% Inputs:
%   
%   ioObj       Object containing information about the parallel port setup
%
%   address     The hexadecimal code for the parallel port
%
%   eventCode   A number (between 1 - 255) signifying the event code to
%               send to the EEG system. 
%
%   pulseDuration  Duration (in seconds) for which to send the EEG event
%                  code
% 
% 
% Outputs:
%
% None
%
% Written by Daniel Feuerriegel, 3/19 at the University of Melbourne


% Use the parallel port to send port codes
io64(ioObj, address, eventCode); % sends the trigger
WaitSecs(pulseDuration); % Duration of the trigger pulse
io64(ioObj, address, 0); % Stop sending a trigger
