function [Port] = set_up_eeg_port(Port)
% 
% This function sets up the parallel port for sending EEG triggers to the
% Biosemi EEG system (or whatever system is being used). The hexadecimal
% port code may need to be adjusted for different computers. This function
% also relies on the inpout.dll device driver, which should already be
% installed on existing stimulus computers. A copy of this dll is provided
% in the experiment code folder just in case.
%
% Note that the event code sending duration is defined within this function
% See: Port.pulseDuration 
%
% Note that the hexadecimal code for the parallel port is defined within
% this function. See: Port.address
%
% Inputs:
%   
%   Port    Structure containing variables relating to sending of event
%           codes to the EEG
%
% 
% Outputs:
%
%   Port    Structure containing variables relating to sending of event
%           codes to the EEG
%
%
% Written by Daniel Feuerriegel for the Change of Mind project at the
% University of Melbourne
% 
 

% Create IO64 object
% from: http://apss.usd.edu/coglab/psych770/IO32.html
% NOTE: Keep the io64.dll file in the current file directory and rename it
% to io64.mexw64 (which is readable by MATLAB). In addition, make sure to
% install inpout64.dll in the C:/windows/system32 directory, or a place
% where windows accessed .dll files.

% io stands for input-output
% This is for setting the folder path of the .dll file - don't need if the
% file is in my experiment folder.
% Important that Matlab's working directory contains the '.dll' file -
% VERY important!! It won't work unless you do this or have the file
% in the system directory.

% NOTE: Change Port.ioObj to ioObj if objects cannot be part of a structure
Port.ioObj = io64;

Port.pulseDuration = 0.002; % Duration of triggers for Biosemi

% Initialise inpout64.dll device driver
if io64(Port.ioObj) == 0

    fprintf('inpout64.dll successfully installed.\n');

else

    fprintf('inpout64.dll installation failed\n');

end % of if io64

% Read the LPT1 status port using io64(ioObj,address)
Port.address = hex2dec('378'); % hex2dec('378'); % LPT1 Status port address
x = io64(Port.ioObj, Port.address); % calling this is VERY important. We found that it doesn't work unless you do!

% Alternatively status = io64(ioObj); % Same thing, confirms that we have the .dll or something!
io64(Port.ioObj, Port.address, 0); % Turn the port off to begin with and then write to it with:
% io64(ioObj,address,port_code);
% remembering to reset it after each pulse with:
% io64(ioObj,address,0); % reset
